#include "Word.hpp"

namespace Echo::SpeechParts
{

    Word::Word(uint32_t id_)
        : id(id_), mfccSize{0} , mfccCoefficients{nullptr}
    {

    }

//    Word::Word(Word &&rhs) noexcept
//    {
//        id               = std::move(rhs.id);
//        mfccSize         = std::move(rhs.mfccSize);
//        mfccCoefficients = std::move(mfccCoefficients);
//
//        rhs.mfccCoefficients = nullptr;
//    }
//
//    Word &Word::operator=(Word &&rhs) noexcept
//    {
//        id               = std::move(rhs.id);
//        mfccSize         = std::move(rhs.mfccSize);
//        mfccCoefficients = std::move(rhs.mfccCoefficients);
//
//        rhs.mfccCoefficients = nullptr;
//    }

    Word::~Word()
    {
//        delete[] mfccCoefficients;
    }

//    Word::Word(const Word &rhs) noexcept
//    {
//        id               = std::move(rhs.id);
//        mfccSize         = std::move(rhs.mfccSize);
//        mfccCoefficients = std::move(mfccCoefficients);
//
//
//    }
//
//    Word &Word::operator=(const Word &rhs) noexcept
//    {
//        id               = std::move(rhs.id);
//        mfccSize         = std::move(rhs.mfccSize);
//        mfccCoefficients = std::move(rhs.mfccCoefficients);
//
//        rhs.mfccCoefficients = nullptr;
//    }


} // namespace Echo::SpeechParts
