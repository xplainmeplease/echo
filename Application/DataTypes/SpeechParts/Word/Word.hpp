#pragma once

#include <cstdint>
#include <bits/move.h>
#include <memory>
#include <utility>
namespace Echo::SpeechParts
{
    class Word
    {
    public:
        explicit Word(uint32_t id_);
//
//        Word(const Word& rhs)            noexcept;
//        Word& operator=(const Word& rhs) noexcept;

//        Word(Word&& rhs)            noexcept;
//        Word& operator=(Word&& rhs) noexcept;

        ~Word();

    public:
        void     initMfcc(const std::shared_ptr<double[]>& mfccCoefficients_,const uint32_t mfccSize_) { mfccCoefficients = mfccCoefficients_; mfccSize = mfccSize_; };

        uint32_t getID()                const noexcept { return id; }
        uint32_t getMfccSize()          const noexcept { return mfccSize; }
        std::shared_ptr<double[]> getMfccCoefficients() const  noexcept { return mfccCoefficients; }
    private:
        uint32_t id;

        uint32_t mfccSize;

        std::shared_ptr<double[]> mfccCoefficients;
    };
} // namespace Echo::SpeechParts





