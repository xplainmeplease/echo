//
// Created by root on 5/20/20.
//


#include <Application/Utilities/Constants.hpp>
#include <Application/Core/Math/MFCC/MFCC.hpp>
#include "Frame.hpp"

namespace Echo::SpeechParts
{

    Frame::Frame() noexcept
        : id{}
    {
    }

    Frame::Frame(const uint32_t _id, const int16_t* source, const std::shared_ptr<double[]>& normalizedSource, const uint32_t start,
                 const uint32_t finish, uint32_t frequency) noexcept
        : id(_id)
    {
        rootMeanSquare  = Echo::Core::Math::CalculateFrameRootMeanSquare(source, start, finish);

        entropy         = Echo::Core::Math::CalculateFrameAverageEntropy(normalizedSource, start, finish,
                                                                  Echo::Utilities::ENTROPY_POSSIBLE_STATES,
                                                                  Echo::Utilities::ENTROPY_DEFAULT_MIN_FRAME_BOUND,
                                                                  Echo::Utilities::ENTROPY_DEFAULT_MAX_FRAME_BOUND);

        mfccCoefficients = Echo::Core::Math::MFCC::transform(normalizedSource, start, finish,
                                                       Echo::Utilities::MFCC_SIZE, frequency,
                                                       Echo::Utilities::MFCC_FREQUENCY_MIN,
                                                       Echo::Utilities::MFCC_FREQUENCY_MAX);
    }

    Frame::~Frame()
    {
//        delete[] mfccCoefficients;
    }

    Frame::Frame(const Frame &rhs) noexcept

    {
        id = rhs.id;
        rootMeanSquare = rhs.rootMeanSquare;
        entropy = rhs.entropy;

        mfccCoefficients = rhs.mfccCoefficients;
    }

    Frame &Frame::operator=(const Frame &rhs) noexcept
    {
        id = rhs.id;
        rootMeanSquare = rhs.rootMeanSquare;
        entropy = rhs.entropy;

        mfccCoefficients = rhs.mfccCoefficients;

        return *this;
    }
} // namespace Echo::SpeechParts