#pragma once

#include <stdint-gcc.h>

#include "Utilities/NonCopyable.hpp"
#include "Application/Core/Math/Math.hpp"
#include <memory>

namespace Echo::SpeechParts
{
    // class Frame : Echo::Utilities::NonCopyable
    class Frame
    {
    public:
        Frame()    noexcept;
        Frame(const uint32_t _id, const int16_t* source, const std::shared_ptr<double[]>& normalizedSource, const uint32_t start, const uint32_t finish, uint32_t frequency) noexcept;

//        Frame(Frame&& rhs)              noexcept;
//        Frame& operator=(Frame&& rhs)   noexcept;
        Frame(const Frame& rhs)              noexcept;
        Frame& operator=(const Frame& rhs)   noexcept;


        ~Frame();

    public:
        uint32_t getID()                const noexcept { return id; }

        double getRootMeanSquare()      const noexcept { return rootMeanSquare; }
        double getEntropy()             const noexcept { return entropy; }

        std::shared_ptr<double[]> getMfccCoefficients()   const noexcept { return mfccCoefficients; }

    private:
        uint32_t id;

        double rootMeanSquare;
        double entropy;

        std::shared_ptr<double[]> mfccCoefficients;
    };
} // namespace Echo::SpeechParts


