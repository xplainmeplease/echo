#pragma once
#include <cstdint>
#include <fstream>
#include <cstring>
#include <iostream>
#include <cmath>

#include <Application/DataTypes/SourceData/SourceData.hpp>


namespace Echo::FileFormats
{
    struct RiffData
    {
        char chunkID[4];
        uint32_t chunkSize;
        char format[4];
    };

    struct FormatDataSubchunk
    {
        char subchunk1ID[4];
        uint32_t subchunk1Size;
        uint16_t audioFormat;
        uint16_t channelsNumber;
        uint32_t sampleRate;
        uint32_t byteRate;
        uint16_t blockAlign;
        uint16_t bitsPerSample;
    };

    struct DataSubchunk
    {
        char subchunk2ID[4];
        uint32_t subchunk2Size;
        char* data;
    };

}

namespace Echo::FileFormats
{
    class Wav
    {
    public:
        Wav() noexcept;

        explicit Wav(const Echo::SourceData& src) noexcept;

//        Wav(const Wav& w)                    noexcept;
//        Wav& operator=(const Wav& rhs)       noexcept;

        Wav(Wav&& w)                    noexcept;
        Wav& operator=(Wav&& rhs)       noexcept;

        ~Wav();

    public:
        RiffData            getRiffData()    const noexcept;
        FormatDataSubchunk  getFormatData()  const noexcept;
        DataSubchunk        getData()        const noexcept;

        uint32_t            getNumberOfSamples() const noexcept;
        std::shared_ptr<double[]> getNormalizedData()  const noexcept;
        int16_t*            getRawData()         const noexcept;


        void                printNormalizedData() const noexcept;
//        void                printRawData()        const noexcept;

    private:
        RiffData            riffData;
        FormatDataSubchunk  formatData;
        DataSubchunk        fileData;

        uint32_t numberOfSamples;

        uint16_t highSoundBound;
        uint16_t  lowSoundBound;

        std::shared_ptr<double[]> normalizedData;
        int16_t* rawData;
    };

    std::ostream& operator<<(std::ostream& os, const Wav& w);
} // namespace Echo::FileFormats

