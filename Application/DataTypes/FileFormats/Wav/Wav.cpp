#include "Wav.hpp"

namespace Echo::FileFormats
{
    Wav::Wav() noexcept
        : highSoundBound{}, lowSoundBound{}, normalizedData{nullptr}
    {

    }

    Wav::Wav(const Echo::SourceData& src) noexcept
        : riffData{}, formatData{}, fileData{}, numberOfSamples{}, normalizedData{nullptr}, highSoundBound{}, lowSoundBound{}
    {
        memcpy(reinterpret_cast<char*>(&riffData),src.getData(),sizeof(riffData));
        memcpy(reinterpret_cast<char*>(&formatData),src.getData() + sizeof(riffData),sizeof(formatData));
        memcpy(reinterpret_cast<char*>(&fileData), src.getData() + sizeof(riffData) + sizeof(formatData),sizeof(fileData.subchunk2ID) + sizeof(fileData.subchunk2Size));

        uint32_t byteOffsetToRawData = sizeof(riffData) + sizeof(formatData) + sizeof(fileData.subchunk2ID) + sizeof(fileData.subchunk2Size);

        uint32_t bytesPerSample = static_cast<uint32_t>(formatData.bitsPerSample / 8);
        uint64_t numberOfSamplesXChannels = fileData.subchunk2Size / (formatData.channelsNumber * bytesPerSample);

        rawData = new int16_t[numberOfSamplesXChannels]; // taking into a count stereo and similar sound,
                                                            // that has more than 1 channel

        char* pointerToSourceData = src.getData()+ byteOffsetToRawData;
//        memcpy(fileData.data,pointerToSourceData,fileData.subchunk2Size);


        uint32_t sampleNumber = 0;
        int16_t sampleValue = 0;
        int16_t minimalSoundBound = 0;
        int16_t maximalSoundBound = 0;
        int16_t value16 = 0; // for mono- sound
        int16_t value16lhs = 0; int16_t value16rhs = 0; // for multiple channels

        while(sampleNumber < numberOfSamplesXChannels)
        {
            if(formatData.channelsNumber == 1)
            {
                memcpy(reinterpret_cast<char*>(&value16), pointerToSourceData, sizeof(value16));
                sampleValue = static_cast<int16_t >(value16);

                pointerToSourceData+=sizeof(value16);
            }
            else
            {
                memcpy(reinterpret_cast<char*>(&value16lhs), pointerToSourceData, sizeof(value16lhs));
                pointerToSourceData+=sizeof(value16lhs);

                memcpy(reinterpret_cast<char*>(&value16rhs), pointerToSourceData, sizeof(value16rhs));
                pointerToSourceData+=sizeof(value16rhs);

                sampleValue = static_cast<int16_t>( (abs(value16lhs) + abs(value16rhs)) / 2);
            }

            if(maximalSoundBound < sampleValue)
                maximalSoundBound = sampleValue;

            if(minimalSoundBound > sampleValue)
                minimalSoundBound = sampleValue;


            rawData[sampleNumber] = sampleValue;

            ++sampleNumber;
        }

        normalizedData = std::shared_ptr<double[]>{new double[sampleNumber], std::default_delete<double[]>()};

        double maxAbsValue = std::max(abs(minimalSoundBound),abs(maximalSoundBound));

        for(uint32_t i = 0; i < sampleNumber; ++i)
        {
            normalizedData[i] = static_cast<double>(rawData[i]) / maxAbsValue;
        }

        numberOfSamples = sampleNumber;

    }

    Wav::Wav(Wav&& w) noexcept
        : riffData(std::move(w.riffData)), formatData(std::move(w.formatData))
    {
        fileData = std::move(w.fileData);
        w.fileData.data = nullptr;
    }

    Wav &Wav::operator=(Wav&& rhs) noexcept
    {
        riffData = std::move(rhs.riffData);
        formatData = std::move(rhs.formatData);

        fileData = std::move(rhs.fileData);
        rhs.fileData.data = nullptr;

        normalizedData = std::move(rhs.normalizedData);
        rhs.normalizedData = nullptr;

        return *this;
    }

    Wav::~Wav()
    {
        delete[] fileData.data;
    }

    RiffData Wav::getRiffData() const noexcept
    {
        return riffData;
    }

    FormatDataSubchunk Wav::getFormatData() const noexcept
    {
        return formatData;
    }

    DataSubchunk Wav::getData() const noexcept
    {
        return fileData;
    }

//    void Wav::printRawData() const noexcept
//    {
//        for(uint32_t i = 0; i < numberOfSamples; ++i)
//        {
//            std::cout << static_cast<int16_t>(fileData.data[i]) << " ";
//            if(i % 10 == 0)
//                std::cout << "\n";
//        }
//        std::cout << "\n";
//    }

    void Wav::printNormalizedData() const noexcept
    {
        for(uint32_t i = 0; i < numberOfSamples; ++i)
        {
            std::cout << normalizedData[i] << " ";
            if(i % 10 == 0)
                std::cout << "\n";
        }
        std::cout << "\n";
    }

    uint32_t Wav::getNumberOfSamples() const noexcept
    {
        return numberOfSamples;
    }

    std::shared_ptr<double[]> Wav::getNormalizedData() const noexcept
    {
        return normalizedData;
    }

    int16_t * Wav::getRawData() const noexcept
    {
        return rawData;
    }

//    Wav::Wav(const Wav& w) noexcept
//    {
//
//    }
//
//    Wav& Wav::operator=(const Wav& rhs) noexcept
//    {
//        riffData = rhs.riffData;
//        formatData = rhs.formatData;
//        formatData = rhs.formatData;
//
//        numberOfSamples = rhs.numberOfSamples;
//
//        highSoundBound = rhs.highSoundBound;
//        lowSoundBound = rhs.lowSoundBound;
//
//        normalizedData = rhs.normalizedData;
//
//        rhs.normalizedData = nullptr;
//
//        return *this;
//    }


    std::ostream &operator<<(std::ostream &os, const Wav &w)
    {
        os << "Chunk ID: " << w.getRiffData().chunkID << "\n";
        os << "Chunk Size: " << w.getRiffData().chunkSize << "\n";
        os << "Format: " << w.getRiffData().format << "\n";

        os << "Chunk ID: " << w.getFormatData().subchunk1ID << "\n";
        os << "Chunk Size: " << w.getFormatData().subchunk1Size << "\n";
        os << "AudioFormat: " << w.getFormatData().audioFormat << "\n";
        os << "ChannelsNumber: " << w.getFormatData().channelsNumber << "\n";
        os << "SampleRate: " << w.getFormatData().sampleRate << "\n";
        os << "ByteRate: " << w.getFormatData().byteRate << "\n";
        os << "BlockAlign: " << w.getFormatData().blockAlign << "\n";
        os << "BitsPerSample: " << w.getFormatData().bitsPerSample << "\n";

        os << "Chunk ID: " << w.getData().subchunk2ID << "\n";
        os << "Chunk Size: " << w.getData().subchunk2Size << "\n";

        return os;
    }
} // namespace Echo::FileFormats
