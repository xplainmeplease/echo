#pragma once

#include <cstdio>
#include <fstream>
#include <memory>


namespace Echo
{
    class SourceData
    {
    public:
        SourceData() noexcept;
        ~SourceData();

        explicit SourceData(std::size_t _length) noexcept;

        SourceData(SourceData&& rhs)       noexcept;
        SourceData& operator=(SourceData&& rhs)       noexcept;

    public:
        char*       getData()   const noexcept;
        std::size_t getLength() const noexcept;

    private:
        char*       data;
        std::size_t length;
    };

    std::ofstream& operator<<(std::ofstream& os,const SourceData& data);

    std::ifstream& operator>>(std::ifstream& is, SourceData& data);



} // namespace Echo



