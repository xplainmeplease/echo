#include <iostream>
#include "SourceData.hpp"

namespace Echo
{
    SourceData::SourceData() noexcept
            : data(nullptr), length{}
    {}

    SourceData::SourceData(std::size_t _length) noexcept
            : length(_length)
    {
        data = new char[length];
    }

    SourceData::SourceData(SourceData&& rhs) noexcept
            : data(std::move(rhs.data)), length(std::move(rhs.length))
    {
        rhs.data   = nullptr;
        rhs.length = 0;
    }

    SourceData& SourceData::operator=(Echo::SourceData &&rhs) noexcept
    {
        data   = std::move(rhs.data);
        length = std::move(rhs.length);

        rhs.data   = nullptr;
        rhs.length = 0;

        return *this;
    }

    SourceData::~SourceData()
    {
        delete[] data;
    }

    char* SourceData::getData() const noexcept
    {
        return data;
    }

    std::size_t SourceData::getLength() const noexcept
    {
        return length;
    }

    std::ofstream &operator<<(std::ofstream &os, const SourceData &data)
    {
        os.write(data.getData(),data.getLength());
        return os;
    }

    std::ifstream &operator>>(std::ifstream &is, SourceData &data)
    {
        is.read(data.getData(),data.getLength());
        return is;
    }
} // namespace Echo





