#include <iostream>

#include <Application/DataTypes/FileFormats/Wav/Wav.hpp>
#include <Application/DataTypes/SourceData/SourceData.hpp>
#include <Application/Core/Controller/TransformationController.hpp>
#include <Application/Core/Storage/Storage.hpp>
#include <Application/Core/Recognition/Model/Model.hpp>
#include <Application/Core/Recognition/RecognitionController/RecognitionController.hpp>
#include <algorithm>
#include <chrono>

void trainMyDragon(const char* path,const char* wordName)
{
    std::cout << "TRAINING HAS STARTED\n";
    std::string tempPath = "../Samples/ToRecognize/";
    tempPath+= path;
    //"male3/3.wav"
    std::ifstream is(tempPath, std::ios_base::binary | std::ios_base::app);
    std::string modelName = wordName;
    if(!is)
    {
        std::cout << "File to train was not opened.\n";
        exit(-1);
    }

    is.seekg(0,std::ios::end);
    std::size_t fileLength = is.tellg();
    is.seekg(0);
    Echo::SourceData src(fileLength);

    is >> src;

    is.close();

    Echo::FileFormats::Wav wav(src);
    Echo::Core::TransformationController controller(&wav);
    Echo::Core::Storage storage{};

    controller.removeShortWords();
    controller.splittDataIntoFrames();
    controller.transformFramesIntoWords();

    std::cout << "Adding a new sample! \n";

    Echo::SpeechParts::Word tempWord = controller.getWholeWord();

    controller.calculateMfccOfTheWord(tempWord);

    Echo::Core::Recognition::Model tempModel{};

    auto models = storage.getModels();
    bool modelWasFound = false;
    for(auto& it: *models)
    {
        if(it.second.getText() == modelName)
        {
            tempModel = it.second;
            modelWasFound = true;
            break;
        }
    }

    if(!modelWasFound)
    {
        tempModel = Echo::Core::Recognition::Model(modelName);
        storage.addModel(tempModel);
    }

    storage.addSampleIntoModel(tempModel.getID(), tempWord);

    std::ofstream os(Echo::Utilities::STORAGE_FILENAME);

    if(!os)
    {
        std::cout << "Storage was not opened!!\n";
        return;
    }

    os << storage;

    std::cout << "\t- New model was successfully added!\n";


}

void tryToRecognize(const char* path)
{
    std::cout << "RECOGNITION HAS STARTED\n";
    std::string tempPath = "../Samples/ToRecognize/";
    tempPath += path;
    std::ifstream is(tempPath , std::ios::binary);

    if(!is)
    {
        std::cout << "File to recognize was not opened\n";
        exit(-1);
    }

    is.seekg(0,std::ios::end);
    std::size_t fileLength = is.tellg();
    is.seekg(0);
    Echo::SourceData src(fileLength);

    is >> src;

    is.close();

    Echo::FileFormats::Wav wav(src);
    Echo::Core::TransformationController controller(&wav);
    Echo::Core::Storage storage{};

    controller.removeShortWords();
    controller.splittDataIntoFrames();
    controller.transformFramesIntoWords();

    std::cout << "Trying to recognize the word:  \n";

    Echo::SpeechParts::Word tempWord = controller.getWholeWord();
    controller.calculateMfccOfTheWord(tempWord);

    std::vector<Echo::Core::Recognition::Model> filteredModels;
    auto models = storage.getModels();

    std::vector<std::string> modelNames;

    std::string tempModelName{};
    for(auto& it: *models)
    {
//        tempModelName = it.second.getText();
//        if(!modelNames.empty() && (std::find(modelNames.begin(),modelNames.end(),modelName) != modelNames.end()) )
//        {
            filteredModels.push_back(it.second);
        //}
    }

    Echo::Core::Recognition::RecognitionController recognitionController(filteredModels);
    auto model = recognitionController.recognize(tempWord);

    if(model.getText() == "Four")
    {
        std::cout << "! Command was successfully recognized. Executing...\n\n";
        system("ping 192.168.0.105 -c 5");
    }

//    std::string outputString = model.getText();
//
//    std::cout << outputString;
}

void printHelp()
{
    std::cout << "Echo says Hello for You!\n";
    std::cout << "If you want to train this dragon. Follow the next pattern:\n";
    std::cout << "\t\t  ./Echo t male1/3.wav Three\n";
    std::cout << "\t\t  ./Echo [option] [name of the file] [word]\n";
    std::cout << "If you want to challenge this dragon. Follow the next pattern:\n";
    std::cout << "\t\t  ./Echo r male1/3.wav\n";
    std::cout << "\t\t  ./Echo [option] [name of the file]\n";
}



int main(int argc, char** argv)
{
    auto  start = std::chrono::system_clock::now();
    if(argc == 2)
    {
        if(strcmp(argv[1],"-h") == 0 )
        {
            printHelp();
            exit(0);
        }
    }

    if(argc >= 3)
    {
        if(strcmp(argv[1],"t") == 0 )
        {
            if(argc < 4)
            {
                std::cout << "Error: Invalid arguments!\n";
                exit(-1);
            }
            trainMyDragon(argv[2],argv[3]);
        }

        else if(strcmp(argv[1],"r") == 0)
        {
            tryToRecognize(argv[2]);
        }

        else
        {
            std::cout << "Error: Invalid arguments!\n";
            exit(-1);
        }
    }
    else
    {
        std::cout << "Error: Invalid arguments!\n";
        exit(-1);
    }
    auto  end = std::chrono::system_clock::now();

    double time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "------------------- "<< time/1000 << "s -------------------\n";
}


