#include <Application/Utilities/Constants.hpp>
#include "TransformationController.hpp"

namespace Echo::Core
{

    TransformationController::TransformationController(Echo::FileFormats::Wav* wavSource) noexcept
        : wordSilenceBound{} , maxRootMeanSquare{}
    {
        wavData = wavSource;

        uint32_t bytesPerFrame  = static_cast<uint32_t>(wavData->getFormatData().byteRate * Echo::Utilities::FRAME_LENGTH / 1000.0);
        uint32_t bytesPerSample = static_cast<uint32_t>(wavData->getFormatData().bitsPerSample / 8);

        samplesPerFrame = static_cast<uint32_t>(bytesPerFrame / bytesPerSample);
    }

    TransformationController::~TransformationController()
    {
//        delete wavData;
    }

    void TransformationController::splittDataIntoFrames() noexcept
    {
        const Echo::FileFormats::Wav* tempWav = getWavData();

        uint32_t samplesWithoutOverlap = static_cast<uint32_t>(samplesPerFrame * (1 - Echo::Utilities::FRAME_OVERLAP));

        uint32_t framesCount = (getWavData()->getData().subchunk2Size / (getWavData()->getFormatData().bitsPerSample / 8) ) / samplesWithoutOverlap;

        uint32_t indexBegin = 0, indexEnd = 0;

        for(uint32_t frameID = 0, numberOfSamples = wavData->getNumberOfSamples(); frameID < framesCount; ++frameID)
        {
            indexBegin = frameID * samplesWithoutOverlap;
            indexEnd = indexBegin + samplesPerFrame;

            if( indexEnd < numberOfSamples )
            {
                Echo::SpeechParts::Frame frame(frameID,getWavData()->getRawData(),getWavData()->getNormalizedData(), indexBegin, indexEnd,getWavData()->getFormatData().sampleRate);

                frames.insert(frames.begin()+frameID, frame);
                frameToRaw.insert(std::make_pair(frameID, std::make_pair(indexBegin, indexEnd)) );
            }
        }

    }

    void TransformationController::transformFramesIntoWords() noexcept
    {
        bool silenceInTheWordExist = checkSilenceExistanceInTheWord();

        uint32_t wordID = -1;

        int64_t currentWordFirstFrameNumber = -1;

        Echo::SpeechParts::Word lastWord(0);

        if(silenceInTheWordExist)
        {
            for(auto it = frames.begin(); it != frames.end(); ++it)
            {
                if(it->getRootMeanSquare() > wordSilenceBound)
                {
                    if (currentWordFirstFrameNumber == -1)
                    {
                        currentWordFirstFrameNumber = it->getID();

                    }
                }
                else
                {
                    if(currentWordFirstFrameNumber >= 0)
                    {
                        uint32_t distance = calculateDistanceBetween2Words(it,lastWord,currentWordFirstFrameNumber,wordID);
                    }
                }
            }

            removeShortWords();
        }
        else
        {
            uniteAllSamplesIntoOneWord();
        }

        if(checkSilenceExistanceInTheWord() && words.size() == 1)
        {
            uniteAllSamplesIntoOneWord();
        }


    }

    uint32_t TransformationController::calculateDistanceBetween2Words(std::vector<Echo::SpeechParts::Frame>::const_iterator it,
                                                                      Echo::SpeechParts::Word& lastWord,
                                                                      int64_t& currentWordFirstFrameNumber,
                                                                      uint32_t& wordID) noexcept
    {
        uint32_t tempDistance{};

        if(lastWord.getID() != 0)
        {
            uint32_t currentWordLastFrameNumber = wordToFrames[lastWord.getID()].second;
            tempDistance = currentWordFirstFrameNumber - currentWordLastFrameNumber;
        }

        //case when we have a new word
        if(lastWord.getID() == 0 || tempDistance >= Echo::Utilities::MIN_DISTANCE_BETWEEN_WORDS)
        {
            wordID++;
            lastWord = Echo::SpeechParts::Word(wordID);

            auto wordFramesBoundsPair = std::make_pair(currentWordFirstFrameNumber, it->getID());
            auto wordIDandFrameBoundsPair = std::make_pair(lastWord.getID(), wordFramesBoundsPair);

            wordToFrames.insert(wordIDandFrameBoundsPair );
            words.push_back(lastWord);
        }

        if(lastWord.getID() != 0 || tempDistance < Echo::Utilities::MIN_DISTANCE_BETWEEN_WORDS)
        {
            double currentWordRootMeanSquare = 0;

            for(uint32_t i = currentWordFirstFrameNumber; i < it->getID(); ++i)
            {
                currentWordRootMeanSquare += frames[i].getRootMeanSquare();
            }

            currentWordRootMeanSquare /= it->getID() - currentWordFirstFrameNumber;

            if(currentWordRootMeanSquare > wordSilenceBound*2)
            {
                uint32_t previousWordFirstFrameNumber = wordToFrames[lastWord.getID()].first;

                wordToFrames.erase(lastWord.getID());

                wordToFrames.insert(std::make_pair(lastWord.getID(), std::make_pair(previousWordFirstFrameNumber, it->getID()) ) );

            }
        }

        currentWordFirstFrameNumber = -1;
        return tempDistance;

    }

    bool TransformationController::checkSilenceExistanceInTheWord() noexcept
    {
        bool silenceExists = false;

        double tempRootMeanSquare, tempMaxRootMeanSquare, tempSilenceRootMeanSquare;

        tempRootMeanSquare = tempMaxRootMeanSquare = frames.at(0).getRootMeanSquare();

        tempSilenceRootMeanSquare = 0;

        uint32_t rootMeanSquareSilenceCount{};

        for(auto& it: frames)
        {
            tempRootMeanSquare = it.getRootMeanSquare();
            tempMaxRootMeanSquare = std::max(tempMaxRootMeanSquare, tempRootMeanSquare);

            if(it.getEntropy() < Echo::Utilities::ENTROPY_SILENCE_BOUND)
            {
                silenceExists = true;
                tempSilenceRootMeanSquare += it.getRootMeanSquare();
                rootMeanSquareSilenceCount++;
            }
        }

        tempSilenceRootMeanSquare /= rootMeanSquareSilenceCount;

        maxRootMeanSquare = tempMaxRootMeanSquare;
        wordSilenceBound = tempSilenceRootMeanSquare * 2;

        return silenceExists;
    }

    void TransformationController::removeShortWords() noexcept
    {
        auto it = words.begin();
        while(it != words.end())
        {
            if(getWordFramesCount(*it) < Echo::Utilities::WORD_MIN_SIZE)
            {
                wordToFrames.erase(it->getID());
                it = words.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

    uint32_t TransformationController::getWordFramesCount(Echo::SpeechParts::Word& word)  noexcept
    {

//        uint32_t difference = wordToFrames[word.getID()].second - wordToFrames[word.getID()].first
        uint32_t difference = wordToFrames[word.getID()].second - wordToFrames[word.getID()].first;
        return difference;
    }

    void TransformationController::uniteAllSamplesIntoOneWord() noexcept
    {
        words.clear();
        wordToFrames.clear();

        Echo::SpeechParts::Word tempWord{0};

        wordToFrames.insert(std::make_pair(tempWord.getID(),
                            std::make_pair(frames.at(0).getID(),frames.at(frames.size()-1).getID())));

        words.push_back(tempWord);
    }

    Echo::SpeechParts::Word TransformationController::getWholeWord() noexcept
    {
        uniteAllSamplesIntoOneWord();
        return std::move(words.at(0));

    }

    void TransformationController::calculateMfccOfTheWord(Echo::SpeechParts::Word &word) noexcept
    {
        uint32_t firstID = wordToFrames[word.getID()].first;
        uint32_t lastID = wordToFrames[word.getID()].second;

        uint32_t framesCount = lastID - firstID + 1;

        std::shared_ptr<double[]> mfcc (new double[Echo::Utilities::MFCC_SIZE * framesCount],std::default_delete<double[]>());

        for(uint32_t i = 0; i < framesCount; ++i)
        {
            uint32_t rawBegin = frameToRaw[firstID+i].first;
            uint32_t rawFinish = frameToRaw[firstID+i].second;

            auto frameMfcc = frames.at(firstID + i).getMfccCoefficients();

            for(uint32_t j = 0; j < Echo::Utilities::MFCC_SIZE; ++j)
            {
                mfcc[i * Echo::Utilities::MFCC_SIZE + j] = frameMfcc[j];
            }

        }

        word.initMfcc(mfcc, Echo::Utilities::MFCC_SIZE * framesCount);
    }




//    TransformationController::TransformationController(TransformationController&& rhs) noexcept
//    {
//
//    }
//
//    TransformationController &TransformationController::operator=(TransformationController&& rhs) noexcept
//    {
//
//
//        return *this;
//    }



} // namespace Echo::Core

