#pragma once

#include <vector>
#include <map>
#include <iostream>

#include <Application/DataTypes/FileFormats/Wav/Wav.hpp>
#include <Application/DataTypes/SpeechParts/Frame/Frame.hpp>
#include <Application/DataTypes/SpeechParts/Word/Word.hpp>


namespace Echo::Core
{
    class TransformationController
    {
    public:
        explicit TransformationController(Echo::FileFormats::Wav* wavSource) noexcept;

//        TransformationController(TransformationController&& rhs) noexcept;
//        TransformationController& operator=(TransformationController&& rhs) noexcept;
        ~TransformationController();

    public:
        void splittDataIntoFrames()             noexcept;
        void transformFramesIntoWords()         noexcept;
        void removeShortWords()                 noexcept;
        void uniteAllSamplesIntoOneWord()       noexcept;
        void calculateMfccOfTheWord(Echo::SpeechParts::Word &word) noexcept;
        bool checkSilenceExistanceInTheWord()   noexcept;

        uint32_t calculateDistanceBetween2Words(std::vector<Echo::SpeechParts::Frame>::const_iterator it, Echo::SpeechParts::Word& lastWord, int64_t& currentWordFirstFrameNumber, uint32_t& wordID) noexcept;
        uint32_t getWordFramesCount(Echo::SpeechParts::Word& word)  noexcept;

        Echo::FileFormats::Wav* getWavData() const noexcept { return wavData; }
        Echo::SpeechParts::Word getWholeWord() noexcept;
//        Echo::SpeechParts::Word getWord() const noexcept;

        double getMaxRootMeanSquare()   const noexcept { return maxRootMeanSquare; }
        double getWordSilenceBound()    const noexcept { return wordSilenceBound; }
        const std::vector<Echo::SpeechParts::Frame>& getFrames() { return frames; }
        const std::vector<Echo::SpeechParts::Word>&  getWords() {return words; }
    public:



    private:
        Echo::FileFormats::Wav* wavData;

        std::vector<Echo::SpeechParts::Frame> frames;
        std::vector<Echo::SpeechParts::Word> words;
//        std::map<uint32_t, Echo::SpeechParts::Frame> frames;
//        std::map<uint32_t, Echo::SpeechParts::Word> words;

        std::map<uint32_t, std::pair<uint32_t, uint32_t> > frameToRaw;
        std::map<uint32_t, std::pair<uint32_t, uint32_t> > wordToFrames;

        uint32_t samplesPerFrame;

        double maxRootMeanSquare;
        double wordSilenceBound;



    };
} // namespace Echo::Core

