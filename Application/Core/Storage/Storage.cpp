#include "Storage.hpp"

namespace Echo::Core
{

    Storage::Storage() noexcept
        : maxID{}
    {
        models = new std::map<uint32_t ,Echo::Core::Recognition::Model>();
        std::cout << "Loading data from the storage:\n";

        std::ifstream is(Echo::Utilities::STORAGE_FILENAME, std::ios::binary);



        if(!is)
        {
            is.close();
            std::cout << "\t- Storage was not opened. Empty one created.\n";

            std::ofstream os(Echo::Utilities::STORAGE_FILENAME, std::ios::binary);
            is.seekg(0,std::ios::end);
            os << maxID;
            os.close();
        }
        else
        {
            std::cout << "\t- Opening storage...\n";

            is >> maxID;
            for(uint32_t i = 0; i < maxID;++i)
            {
                auto tempModel = Echo::Core::Recognition::Model();
                is >> tempModel;

                models->insert(std::make_pair(tempModel.getID(),tempModel));
            }
            std::cout<< "\t- Storage was closed\n";
            is.close();
        }



    }

    uint32_t Storage::getMaxID() const noexcept
    {
        return maxID;
    }

    const std::map<uint32_t, Echo::Core::Recognition::Model>* Storage::getModels() const noexcept
    {
        return models;
    }

    void Storage::addModel(Echo::Core::Recognition::Model& model) noexcept
    {
        model.setID(++maxID);
        models->insert(std::make_pair(maxID,model));
    }

    void Storage::addSampleIntoModel(uint32_t modelID,const Echo::SpeechParts::Word &word) noexcept
    {
        //Echo::Core::Recognition::Model* model = &models.at(modelID);
        Echo::Core::Recognition::MFCCSample tempSample{ word.getMfccSize() };
        tempSample.setCoefficients( word.getMfccCoefficients() );

        models->at(modelID).addSample(tempSample);
    }

    Storage::~Storage()
    {
        delete models;
    }

    ::std::ofstream& operator<<(::std::ofstream& os,const Storage& storage)
    {
        std::cout << "\t - Saving storage.\n";

        os << storage.getMaxID();

        auto tempModels = storage.getModels();

        for(auto& it : *tempModels)
        {
            os << it.second;
        }
        std::cout << "\t - Saving storage is finished!\n";

        return os;
    }


} // namespace Echo::Core

