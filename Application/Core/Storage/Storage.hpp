#pragma once

#include <map>
#include <cassert>
#include <iostream>
#include <Application/Utilities/Constants.hpp>
#include <Application/Core/Recognition/Model/Model.hpp>
#include <Application/DataTypes/SpeechParts/Word/Word.hpp>

namespace Echo::Core
{
    class Storage
    {
    public:
        Storage() noexcept;
        ~Storage();
        uint32_t getMaxID() const noexcept;
        const std::map<uint32_t ,Echo::Core::Recognition::Model>* getModels() const noexcept;

        void addModel(Echo::Core::Recognition::Model& model) noexcept;
        void addSampleIntoModel(uint32_t modelID, const Echo::SpeechParts::Word &word) noexcept;

    private:
        uint32_t maxID;

        std::map<uint32_t ,Echo::Core::Recognition::Model>* models;
    };

    ::std::ofstream& operator<<(::std::ofstream& os,const Storage& storage);

} // namespace Echo::Core





