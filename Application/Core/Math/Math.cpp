#include <memory>
#include "Math.hpp"

namespace Echo::Core::Math
{

    double CalculateFrameAverageEntropy(const std::shared_ptr<double[]>& normilizedSource, const uint32_t start, const uint32_t finish,
                                        const uint8_t entropyPossibleStates, const double frameMinBound,
                                        const double frameMaxBound) noexcept
    {
        double tmpEntropy{};

        double binSize = fabs(frameMaxBound - frameMinBound) / static_cast<double>(entropyPossibleStates);

        if (fabs(binSize) < std::numeric_limits<double>::epsilon())
        {
            return 0;
        }

        double* probabilities = new double[entropyPossibleStates];
        for(uint8_t i = 0; i < entropyPossibleStates; ++i)
        {
            probabilities[i] = 0.;
        }

        uint8_t index;

        for(uint32_t i = start; i <= finish; ++i)
        {
            double tmpFrameValue = normilizedSource[i];
            index = floor((tmpFrameValue - frameMinBound) / binSize);

            if(index >= entropyPossibleStates)
            {
                index = entropyPossibleStates - 1;
            }

            probabilities[index] += 1.;
        }

        // normalize probabilities

        uint8_t size = finish - start + 1;
        for(uint8_t i = 0; i < entropyPossibleStates; ++i)
        {
            probabilities[i] /= size;
        }

        for(uint8_t i = 0; i < entropyPossibleStates; ++i)
        {
            if(probabilities[i] > std::numeric_limits<double>::epsilon())
            {
                tmpEntropy += probabilities[i] * log2(probabilities[i]);
            }
        }


        delete[] probabilities;

        tmpEntropy = -tmpEntropy;
        return tmpEntropy;
    }

    double CalculateFrameRootMeanSquare(const int16_t *source, const uint32_t start, const uint32_t finish) noexcept
    {
        double tmpRMS{};

        for(uint32_t i = start; i <= finish; ++i)
        {
            tmpRMS += source[i] * source[i];
        }
        tmpRMS /= (finish-start) + 1;

        return tmpRMS;
    }

} // namespace Echo::Core::Math
