#pragma once

#include "MathIncludes.hpp"

namespace Echo::Core::Math
{

    double CalculateFrameRootMeanSquare(const int16_t* source,const uint32_t start, const uint32_t finish) noexcept;

    double CalculateFrameAverageEntropy(const std::shared_ptr<double[]>& normilizedSource,const uint32_t start, const uint32_t finish, const uint8_t entropyStates,
                                        const double frameMinBound,const double frameMaxBound) noexcept;
} // namespace Echo::Core::Math
