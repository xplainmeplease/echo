#include <iostream>
#include <memory>
#include "DTW.hpp"

namespace Echo::Core::Math::DTW
{
//    double calculateDistance(double* firstSequence, uint32_t firstSequenceSize, double* secondSequence, uint32_t secondSequenceSize)
//    {
//
//        // Create diff matrix
//        if(firstSequenceSize == 0)
//        {
//            firstSequenceSize = 1;
//        }
//
//        double** differenceMatrix = new double*[firstSequenceSize];
//
//        for (uint32_t i = 0; i < firstSequenceSize; i++)
//        {
//            differenceMatrix[i] = new  double[secondSequenceSize];
//        }
//
//        for (uint32_t i = 0; i < firstSequenceSize; i++)
//        {
//            for (uint32_t j = 0; j < secondSequenceSize; j++)
//            {
//                differenceMatrix[i][j] = fabs(firstSequence[i] - secondSequence[j]);
//            }
//        }
//
//        // Compute distance
//        double distance = findDistance(firstSequenceSize, secondSequenceSize, differenceMatrix);
//
//        // Clean up
//        for (uint32_t i = 0; i < firstSequenceSize; i++)
//        {
//            delete [] differenceMatrix[i];
//        }
//        delete [] differenceMatrix;
//
//        return distance;
//    }

    double calculateDistanceVector(const std::shared_ptr<double[]>& firstSequence, uint32_t firstSequenceSize, const std::shared_ptr<double[]>& secondSequence, uint32_t secondSequenceSize, uint8_t vectorSize)
    {
        std::cout << "\t- Start: calculating distance.\n";
        uint32_t firstSequenceToVectorSizeRatio  = firstSequenceSize  / vectorSize;
        uint32_t secondSequenceToVectorSizeRatio = secondSequenceSize / vectorSize;

        // Create difference matrix
        if(firstSequenceToVectorSizeRatio == 0)
        {
            firstSequenceToVectorSizeRatio = 1;
        }
        double** differenceMatrix = new double*[firstSequenceToVectorSizeRatio];
        for (uint32_t i = 0; i < firstSequenceToVectorSizeRatio; i++)
        {
            differenceMatrix[i] = new  double[secondSequenceToVectorSizeRatio];
        }

        for (uint32_t i = 0; i < firstSequenceToVectorSizeRatio; i++)
        {
            for (uint32_t j = 0; j < secondSequenceToVectorSizeRatio; j++)
            {

                // Calc distance between vectors
                double distance = 0.;
                for (uint32_t k = 0; k < vectorSize; k++)
                {
                    distance += pow(firstSequence[i * vectorSize + k] - secondSequence[j * vectorSize + k], 2);
                }

                differenceMatrix[i][j] = sqrt(distance);
            }
        }

        // Compute distance
        double distance = findDistance(firstSequenceToVectorSizeRatio, secondSequenceToVectorSizeRatio, differenceMatrix);

        // Clean up
        for (uint32_t i = 0; i < firstSequenceToVectorSizeRatio; i++)
        {
            delete [] differenceMatrix[i];
        }
        delete [] differenceMatrix;

        std::cout << "\t- End  : calculating distance. Distance = " << distance << "\n";
        return distance;
    }

    double findDistance(uint32_t firstSequenceSize, uint32_t secondSequenceSize, double** differenceMatrix)
    {
        if(firstSequenceSize == 0)
        {
            firstSequenceSize = 1;
        }
        // Create distance matrix (forward direction)
        double** pathMatrix = new double*[firstSequenceSize];


        for (uint32_t i = 0; i < firstSequenceSize; i++)
        {
            pathMatrix[i] = new double[secondSequenceSize];
        }


        pathMatrix[0][0] = differenceMatrix[0][0];

        for (uint32_t i = 1; i < firstSequenceSize; i++)
        {
            pathMatrix[i][0] = differenceMatrix[i][0] + pathMatrix[i - 1][0];
        }

        for (uint32_t j = 1; j < secondSequenceSize; j++)
        {
            pathMatrix[0][j] = differenceMatrix[0][j] + pathMatrix[0][j - 1];
        }

        for (uint32_t i = 1; i < firstSequenceSize; i++)
        {
            for (uint32_t j = 1; j < secondSequenceSize; j++)
            {
                if (pathMatrix[i - 1][j - 1] < pathMatrix[i - 1][j])
                {
                    if (pathMatrix[i - 1][j - 1] < pathMatrix[i][j - 1])
                    {
                        pathMatrix[i][j] = differenceMatrix[i][j] + pathMatrix[i - 1][j - 1];
                    }
                    else
                    {
                        pathMatrix[i][j] = differenceMatrix[i][j] + pathMatrix[i][j - 1];
                    }
                }
                else
                {
                    if (pathMatrix[i - 1][j] < pathMatrix[i][j - 1])
                    {
                        pathMatrix[i][j] = differenceMatrix[i][j] + pathMatrix[i - 1][j];
                    }
                    else
                    {
                        pathMatrix[i][j] = differenceMatrix[i][j] + pathMatrix[i][j - 1];
                    }
                }
            }
        }

        // Find the warping path (backward direction)
        if(firstSequenceSize == 0)
            firstSequenceSize = 1;

        uint32_t warpSize = firstSequenceSize * secondSequenceSize;
        double* warpPath = new double[warpSize];

        uint32_t warpPathIndex = 0;
        uint32_t firstSequenceSizeCounter = firstSequenceSize - 1;
        uint32_t secondSequenceSizeCounter = secondSequenceSize - 1;

        warpPath[warpPathIndex] = pathMatrix[firstSequenceSizeCounter][secondSequenceSizeCounter];
        //!!!!!!!!!!!!!!!!!!!!!!!!!!DEBUG("Path (%d, %d): %f", i, j, pathM[i][j]);

        do
        {
            if (firstSequenceSizeCounter > 0 && secondSequenceSizeCounter > 0)
            {

                if (pathMatrix[firstSequenceSizeCounter - 1][secondSequenceSizeCounter - 1] < pathMatrix[firstSequenceSizeCounter - 1][secondSequenceSizeCounter])
                {
                    if (pathMatrix[firstSequenceSizeCounter - 1][secondSequenceSizeCounter - 1] < pathMatrix[firstSequenceSizeCounter][secondSequenceSizeCounter - 1])
                    {
                        firstSequenceSizeCounter--;
                        secondSequenceSizeCounter--;
                    }
                    else
                    {
                        secondSequenceSizeCounter--;
                    }

                }
                else
                {
                    if (pathMatrix[firstSequenceSizeCounter - 1][secondSequenceSizeCounter] < pathMatrix[firstSequenceSizeCounter][secondSequenceSizeCounter - 1])
                    {
                        firstSequenceSizeCounter--;
                    }
                    else
                    {
                        secondSequenceSizeCounter--;
                    }
                }

            }
            else
            {
                if (0 == firstSequenceSizeCounter)
                {
                    secondSequenceSizeCounter--;
                }
                else
                {
                    firstSequenceSizeCounter--;
                }
            }

            warpPath[++warpPathIndex] = pathMatrix[firstSequenceSizeCounter][secondSequenceSizeCounter];
            //!!!!!!!!!!!!!!!!!!!!!!!!!!DEBUG("Path (%d, %d): %f", i, j, pathM[i][j]);

        } while (firstSequenceSizeCounter > 0 || secondSequenceSizeCounter > 0);

        // Calculate path measure
        double distance = 0.;
        for (uint32_t k = 0; k < warpPathIndex + 1; k++)
        {
            distance += warpPath[k];
        }
        distance = distance / (warpPathIndex + 1);

        // Clean up
        delete[] warpPath;
        for (uint32_t i = 0; i < firstSequenceSize; i++)
        {
            delete[] pathMatrix[i];
        }
        delete[] pathMatrix;

        return distance;
    }
} // namespace Echo::Core::Math::DTW