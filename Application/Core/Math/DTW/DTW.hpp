#pragma once

#include "../MathIncludes.hpp"

namespace Echo::Core::Math::DTW
{

    double calculateDistance(double* firstSequence, uint32_t firstSequenceSize, const std::shared_ptr<double[]>& secondSequence, uint32_t secondSequenceSize);


    // Calculate distance between two sequences of vectors
    double calculateDistanceVector(const std::shared_ptr<double[]>& firstSequence, uint32_t firstSequenceSize, const std::shared_ptr<double[]>& secondSequence, uint32_t secondSequenceSize, uint8_t vectorSize);

    double findDistance(uint32_t firstSequenceSize, uint32_t secondSequenceSize, double** differenceMatrix);
} // namespace Echo::Core::Math::DTW