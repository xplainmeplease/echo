#pragma once

#include "../MathIncludes.hpp"

namespace Echo::Core::Math::MFCC
{
    std::shared_ptr<double[]> transform(const std::shared_ptr<double[]>& source, uint32_t start, uint32_t finish, uint8_t mfccSize, uint32_t frequency, uint32_t minimalFrequency, uint32_t maximalFrequency);

    //double* filter(const double* source, uint32_t start, uint32_t finish);

    std::shared_ptr<double[]> fourierTransform(const std::shared_ptr<double[]>& source, uint32_t start, uint32_t length, bool useWindow);

    double** getMelFilters(uint8_t mfccSize, uint32_t filterLength, uint32_t frequency, uint32_t minimalFrequency, uint32_t maximalFrequency);

    std::shared_ptr<double[]> calcPower(const std::shared_ptr<double[]>& fourierRaw, uint32_t fourierLength, double** melFilters, uint8_t mfccSize);

    std::shared_ptr<double[]> dctTransform(const std::shared_ptr<double[]>& data, uint32_t length);

    double convertToMel(double f);
    double convertFromMel(double m);

} // namespace Echo::Core::Math::MFCC