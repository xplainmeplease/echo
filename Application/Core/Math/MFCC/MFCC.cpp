#include <memory>
#include "MFCC.hpp"

namespace Echo::Core::Math::MFCC
{

    std::shared_ptr<double[]> transform(const std::shared_ptr<double[]>& source, uint32_t start, uint32_t finish, uint8_t mfccSize,
                      uint32_t frequency, uint32_t minimalFrequency, uint32_t maximalFrequency)
    {
        uint32_t sampleLength = finish - start + 1;

        // Calc
        std::shared_ptr<double[]> fourierRaw = fourierTransform(source, start, sampleLength, true);
        double** melFilters = getMelFilters(mfccSize, sampleLength, frequency, minimalFrequency, maximalFrequency);
        std::shared_ptr<double[]> logPower = calcPower(fourierRaw, sampleLength, melFilters, mfccSize);
        std::shared_ptr<double[]> dctRaw = dctTransform(logPower, mfccSize);




        for (unsigned short m = 0; m < mfccSize; m++) {
            delete [] melFilters[m];
        }
        delete [] melFilters;

        return dctRaw;
    }

//    double* filter(const double* source, uint32_t start, uint32_t finish)
//    {
//        // nothing is needed
//    }

    // Compute singnal's spectrum and its magnitudes (short-time Fourier transform with Hamming window)
    std::shared_ptr<double[]> fourierTransform(const std::shared_ptr<double[]>& source, uint32_t start, uint32_t length, bool useWindow)
    {
        std::complex<double>* fourierCmplxRaw = new std::complex<double>[length];

        std::shared_ptr<double[]> fourierRaw  (new double[length], std::default_delete<double[]>());


        for (uint32_t k = 0; k < length; k++)
        {
            fourierCmplxRaw[k] = std::complex<double>(0, 0);

            for (uint32_t n = 0; n < length; n++)
            {
                double sample = source[start + n];

                // According Euler's formula: e^(ix) = cos(x) + i*sin(x)
                double x = -2. * M_PI * k * n / (double) length;
                std::complex<double> f = sample * std::complex<double>(cos(x), sin(x));

                double w = 1.;
                if (useWindow)
                {
                    // Hamming window
                    w = 0.54 - 0.46 * cos(2 * M_PI * n / (length - 1));
                }

                fourierCmplxRaw[k] += f * w;
            }

            // As for magnitude, let's use Euclid's distance for its calculation
            fourierRaw[k] = sqrt(norm(fourierCmplxRaw[k]));
        }

        delete [] fourierCmplxRaw;

        return fourierRaw;
    }


    // Create triangular filters spaced on mel scale
    double** getMelFilters(uint8_t mfccSize, uint32_t filterLength, uint32_t frequency, uint32_t minimalFrequency, uint32_t maximalFrequency)
    {

        // Create pointers for filter banks
        double* filterBankPointers = new double[mfccSize + 2];
        filterBankPointers[0] = convertToMel(minimalFrequency);
        filterBankPointers[mfccSize + 1] = convertToMel(maximalFrequency);

        // Create mel bin
        for (unsigned short m = 1; m < mfccSize + 1; m++)
        {
            filterBankPointers[m] = filterBankPointers[0] + m * (filterBankPointers[mfccSize + 1] - filterBankPointers[0]) / (mfccSize + 1);
        }

        //frequency = 0.5 * frequency;
        for (unsigned short m = 0; m < mfccSize + 2; m++)
        {

            // Convert them from mel to frequency
            filterBankPointers[m] = convertFromMel(filterBankPointers[m]);

            // Map those frequencies to the nearest FT bin
            filterBankPointers[m] = floor((filterLength + 1) * filterBankPointers[m] / (double) frequency);

            assert("FT bin too small" && !(m > 0 && (filterBankPointers[m] - filterBankPointers[m - 1]) < std::numeric_limits<double>::epsilon()));
        }

        // Calc filter banks
        double** filterBanks = new double*[mfccSize];
        for (unsigned short m = 0; m < mfccSize; m++)
        {
            filterBanks[m] =  new double[filterLength];
        }

        for (unsigned short m = 1; m < mfccSize + 1; m++)
        {
            for (uint32_t k = 0; k < filterLength; k++)
            {

                if (filterBankPointers[m - 1] <= k && k <= filterBankPointers[m])
                {
                    filterBanks[m - 1][k] = (k - filterBankPointers[m - 1]) / (filterBankPointers[m] - filterBankPointers[m - 1]);

                }
                else if (filterBankPointers[m] < k && k <= filterBankPointers[m + 1])
                {
                    filterBanks[m - 1][k] = (filterBankPointers[m + 1] - k) / (filterBankPointers[m + 1] - filterBankPointers[m]);

                }
                else
                {
                    filterBanks[m - 1][k] = 0;
                }
            }
        }

        delete [] filterBankPointers;

        return filterBanks;
    }


    // Apply mel filters to spectrum's magnitudes, take the logs of the powers

    std::shared_ptr<double[]> calcPower(const std::shared_ptr<double[]>& fourierRaw, uint32_t fourierLength,
                      double** melFilters, uint8_t mfccCount)
    {

        std::shared_ptr<double[]> logPower(new double[mfccCount],std::default_delete<double[]>());

        for (uint16_t m = 0; m < mfccCount; m++)
        {
            logPower[m] = 0.;

            for (uint32_t k = 0; k < fourierLength; k++)
            {
                logPower[m] += melFilters[m][k] * pow(fourierRaw[k], 2);
            }

            assert("Spectrum power is less than zero" && logPower[m] >= std::numeric_limits<double>::epsilon());

            // possibly is not needed(bcs of normalized data)
            logPower[m] = log(logPower[m]);
        }

        return logPower;
    }


    // Take the discrete cosine transform of the list of mel log powers
    std::shared_ptr<double[]> dctTransform(const std::shared_ptr<double[]>& data, uint32_t length)
    {

        std::shared_ptr<double[]> dctTransform(new double[length],std::default_delete<double[]>());

        for (unsigned short n = 0; n < length; n++)
        {
            dctTransform[n] = 0;

            for (unsigned short m = 0; m < length; m++)
            {
                dctTransform[n] += data[m] * cos(M_PI * n * (m + 1./2.) / length);
            }
        }

        return dctTransform;
    }

    double convertToMel(double f)
    {
        return 1125. * log(1. + f/700.);
    }

    double convertFromMel(double m)
    {
        return 700. * (exp(m/1125.) - 1);
    }

} // namespace Echo::Core::Math::MFCC