#include "Model.hpp"

namespace Echo::Core::Recognition
{
    Model::Model() noexcept
        :id{}, text{}
    {

    }

    Model::Model(std::string text_) noexcept
        :text(std::move(text_)), id(0)
    {

    }

    std::string Model::getText() const noexcept
    {
        return text;
    }

    uint32_t Model::getID() const noexcept
    {
        return id;
    }

    const std::vector<MFCCSample> &Model::getSamples() const noexcept
    {
        return samples;
    }

    void Model::setID(uint32_t id_) noexcept
    {
        id = id_;
    }

    void Model::addSample(const MFCCSample& sample) noexcept
    {
        samples.push_back(sample);
    }

    void Model::setText(std::string text_) noexcept
    {
        text = std::move(text_);
    }

    Model::Model(const Model &model) noexcept
    {
        id = model.id;

        text = model.text;

        samples = model.samples;
    }

    Model &Model::operator=(const Model &model) noexcept
    {
        id = model.id;

        text = model.text;

        samples = model.samples;

        return *this;
    }

    Model::operator bool() const noexcept
    {
        return id == 0 && text.empty() && samples.empty();
    }

    Model::~Model() = default;


} // namespace Echo::Core::Recognition

namespace Echo::Core::Recognition
{
    ::std::ostream&  operator<<(::std::ofstream& os,const Model& model)
    {
        uint32_t tempID = model.getID();
        uint32_t tempModelNameSize = model.getText().size() + 1;
        uint32_t tempSamplesSize = model.getSamples().size();
        uint32_t tempCertainSampleSize{};

        os.write( reinterpret_cast<char*>(&tempID) ,sizeof(tempID) );
        os.write( reinterpret_cast<char*>(&tempModelNameSize) ,sizeof(tempModelNameSize) );
        os.write( model.getText().c_str() ,sizeof(model.getText().size() + 1) );
        os.write( reinterpret_cast<char*>(&tempSamplesSize), sizeof(tempSamplesSize) );
//        os << model.getID();
//        os << model.getText().size() + 1;
//        os << model.getText();
//        os << model.getSamples().size();

        for(auto& it: model.getSamples())
        {
            tempCertainSampleSize = it.getSize();
            os.write( reinterpret_cast<char*>(&tempCertainSampleSize), sizeof(tempCertainSampleSize) );
//            os << it.getSize();
            os.write(reinterpret_cast<char*>(it.getCoefficients().get()),it.getSize() * sizeof( *(it.getCoefficients().get()) ));
        }

        return os;
    }

    ::std::ifstream& operator>>(::std::ifstream& is,      Model& model)
    {
        uint32_t tempID{};
        uint32_t tempTextSize{};
        uint32_t tempSamplesCount{};
        char* tempText = nullptr;

        is.read(reinterpret_cast<char*>(&tempID),sizeof(tempID));
        is.read(reinterpret_cast<char*>(&tempTextSize),sizeof(tempTextSize));

        tempText = new char[tempTextSize];

        is.read(tempText,sizeof(tempText));
        is.read(reinterpret_cast<char*>(&tempSamplesCount),sizeof(tempSamplesCount));

        for(uint32_t i = 0; i < tempSamplesCount; ++i)
        {
            uint32_t tempSampleSize{};
            is.read(reinterpret_cast<char*>(&tempSampleSize),sizeof(tempSampleSize));


            std::shared_ptr<double[]> tempSampleData (new double[tempSampleSize], std::default_delete<double[]>());
            is.read(reinterpret_cast<char*>(tempSampleData.get()),tempSampleSize * sizeof( double ));
//            is.read(reinterpret_cast<char*>(tempSampleData),tempSampleSize * sizeof( *(tempSampleData) ));

            Recognition::MFCCSample tempSample{tempSampleSize};
            tempSample.setCoefficients(tempSampleData);

            tempSampleData = nullptr;

            model.addSample(tempSample);
        }

        model.setID(tempID);
        std::string tempstr(tempText);
        model.setText(tempstr);

        delete[] tempText;

        return is;
    }
} // namespace Echo::Core::Recognition

