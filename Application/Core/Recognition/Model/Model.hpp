#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

namespace Echo::Core::Recognition
{
    class MFCCSample
    {
    public:
        explicit MFCCSample(uint32_t size_) noexcept
            : size{size_}
        {
            coefficients = std::move(std::shared_ptr<double[]>{new double[size],std::default_delete<double[]>()});
            // lmao
        };

        ~MFCCSample() = default;


        void setCoefficients(const std::shared_ptr<double[]>& mfccData) noexcept
        {
            for(uint32_t i = 0; i < size; ++i)
            {
                coefficients[i] = mfccData[i];
            }
        }

        uint32_t getSize()          const noexcept { return size; }
        std::shared_ptr<double[]>  getCoefficients()  const noexcept { return coefficients; }

    private:
        uint32_t size;
        std::shared_ptr<double[]> coefficients;
    };


    class Model
    {
    public:
        Model() noexcept;
        explicit Model(std::string text) noexcept;

        Model(const Model& model) noexcept;
        Model& operator=(const Model& model) noexcept;

        explicit operator bool() const noexcept;

        ~Model();

    public:
        std::string getText()   const noexcept;
        uint32_t    getID()     const noexcept;

        const std::vector<MFCCSample>& getSamples() const noexcept;

        void setID(uint32_t id) noexcept;
        void setText(std::string text_) noexcept;

        void addSample(const MFCCSample &sample) noexcept;

    private:
        uint32_t id;

        std::string text;

        std::vector<MFCCSample> samples;

    };

    ::std::ifstream& operator>>(::std::ifstream& os,      Model& model);
    ::std::ostream&  operator<<(::std::ofstream& os,const Model& model);


} // namespace Echo::Core::Recognition



