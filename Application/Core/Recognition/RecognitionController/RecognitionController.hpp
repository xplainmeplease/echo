#pragma once

#include <vector>
#include <iostream>
#include <Application/Core/Math/DTW/DTW.hpp>
#include <Application/Utilities/Constants.hpp>
#include <Application/Core/Recognition/Model/Model.hpp>
#include <Application/DataTypes/SpeechParts/Word/Word.hpp>


namespace Echo::Core::Recognition
{
    class RecognitionController
    {
    public:
        explicit RecognitionController(const std::vector<Echo::Core::Recognition::Model>& models_) noexcept;

        Model recognize(const Echo::SpeechParts::Word& word) noexcept;
    private:
        std::vector<Echo::Core::Recognition::Model> models;
    };

}


