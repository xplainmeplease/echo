
#include "RecognitionController.hpp"

namespace Echo::Core::Recognition
{

    RecognitionController::RecognitionController(const std::vector<Echo::Core::Recognition::Model> &models_) noexcept
    {
        models = models_;
    }

    Model RecognitionController::recognize(const Echo::SpeechParts::Word &word) noexcept
    {
        Model bestModel;

        double zeroBound = 0.;

        for(auto& model : models)
        {
            double tempDistance = 0.;

            for(auto& sample : model.getSamples())
            {
                tempDistance += Echo::Core::Math::DTW::calculateDistanceVector(word.getMfccCoefficients(),
                                                                               word.getMfccSize(),
                                                                               sample.getCoefficients(),
                                                                               sample.getSize(),
                                                                               Echo::Utilities::MFCC_SIZE);
            }

            tempDistance /= model.getSamples().size();
            std::cout << "Distance for model: |" << model.getText() << " | = " << tempDistance << "\n";

            if(static_cast<bool>(bestModel) || tempDistance < zeroBound)
            {
                zeroBound = tempDistance;
                bestModel = model;
            }
        }

        std::cout << "Recognized word is: " << bestModel.getText() << ". Distance = " << zeroBound << "\n";

        return bestModel;
    }

}