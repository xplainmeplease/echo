#pragma once

#include <stdint-gcc.h>

namespace Echo::Utilities
{
    const char STORAGE_FILENAME[] = "/root/cpp/Echo/Application/models.dat";

    const uint8_t FRAME_LENGTH = 10;
    const double FRAME_OVERLAP = 0.5;

    const uint16_t WORD_MIN_SIZE = (200 / FRAME_LENGTH) / (1 - FRAME_OVERLAP);
    const uint16_t MIN_DISTANCE_BETWEEN_WORDS = WORD_MIN_SIZE * 0.5;

    const uint8_t ENTROPY_POSSIBLE_STATES = 75;
    const double ENTROPY_SILENCE_BOUND = 0.1;
    const double ENTROPY_DEFAULT_MAX_FRAME_BOUND = 1;
    const double ENTROPY_DEFAULT_MIN_FRAME_BOUND = -1;

    const uint16_t MFCC_SIZE = 12;
    const uint16_t MFCC_FREQUENCY_MIN = 300;
    const uint16_t MFCC_FREQUENCY_MAX = 4000;
} // namespace Echo::Utilities