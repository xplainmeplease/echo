#pragma  once


namespace Echo::Utilities
{
    class NonCopyable
    {
        public:
            NonCopyable() = default;
            ~NonCopyable() = default;

            NonCopyable(const NonCopyable&) = delete;
            NonCopyable& operator=(const NonCopyable&) = delete;
    };
} // namespace Echo::Utilities


